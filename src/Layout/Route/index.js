import { Route, Routes } from 'react-router-dom';

const RouteLayout = ({ component: Component, layout: Layout, ...rest }) => {
    return (
        <Routes>
            <Route 
                {...rest} 
                element={
                    <Layout>
                        <Component />
                    </Layout>
                }
            />
        </Routes>
    );
};

export default RouteLayout;