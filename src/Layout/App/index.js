import {
  HomeOutlined,
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    UnorderedListOutlined,
    UploadOutlined,
    UserOutlined,
    VideoCameraOutlined,
  } from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import React, { memo, useState } from 'react';
import styles from './style.module.scss';
import { useNavigate } from "react-router-dom";

const { Header, Sider, Content } = Layout;

const AppLayout = memo((props) => {
  const navigate = useNavigate();
  const [collapsed, setCollapsed] = useState(false);
  return (
    <Layout className={styles.layoutCustom}>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo" />
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={['/']}
          onClick={(item)=>{ 
            navigate("/" + item.key, { replace: true });
          }}
          items={[
            {
              key: '',
              icon: <HomeOutlined />,
              label: 'Home',
            },
            {
              key: 'todos',
              icon: <UnorderedListOutlined />,
              label: 'Todo List',
            },
            {
              key: 'page1',
              icon: <UserOutlined />,
              label: 'Page 01',
            },
            {
              key: 'page2',
              icon: <VideoCameraOutlined />,
              label: 'Page 02',
            },
            {
              key: 'page3',
              icon: <UploadOutlined />,
              label: 'Page 03',
            },
          ]}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: 'trigger',
            onClick: () => setCollapsed(!collapsed),
          })}
        </Header>
        <Content
          className="site-layout-background"
          style={{
            margin: '24px 16px',
            padding: 24,
            minHeight: 280,
          }}
        >
          {props.children}
        </Content>
      </Layout>
    </Layout>
  );
});

export default AppLayout;