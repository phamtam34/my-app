import { Table } from 'antd';
import axios from 'axios';
import useFetch from '../../Hooks/useFetch';

const TodoContainer = () => {
    const {data, loading, error, refetch} = useFetch("/todos");    
    
    const columns = [
        { title: 'Name', dataIndex: 'name' },        
        { title: 'Age', dataIndex: 'age' },
        { title: 'Address', dataIndex: 'address' }
    ];

    console.log(data);

    return (
        <>
            <Table
                loading={loading}
                columns={columns}
                dataSource={data}
                pagination={false}
            />
        </>
    )
};

export default TodoContainer;