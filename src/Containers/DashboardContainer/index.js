import { Steps } from "antd";
import Card from "../Steps/Card";
import Confirm from "../Steps/Confirm";
import Person from "../Steps/Person";

const { Step } = Steps;

const DashboardContainer = () => {
    return (
        <>
            <Steps current={0}>
                <Step title="個人情報入力" />
                <Step title="カード情報入力" />
                <Step title="確認" />
                <Step title="完了" />
            </Steps>
            {content(0)}
        </>
        
    )
};

export default DashboardContainer;


export function content(step) {
    switch (step) {
    case 0:
      return <Person />;
    case 1:
      return <Card />;
    case 2:
      return <Confirm />;
    default:
      throw new Error('Unknown step');
    }
}