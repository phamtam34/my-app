import AppRouter from "../Routes"

const WrapApp = () => {
    return <AppRouter />
};

export default WrapApp;