import { BrowserRouter } from "react-router-dom";
import WrapApp from "./WrapApp";

function App() {
  return (
    <BrowserRouter>   
      <WrapApp />   
    </BrowserRouter>
  );
}

export default App;
