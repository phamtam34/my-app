import { useEffect, useState } from "react"
import { axios } from "../Utils/axios"


const useFetch = (url) =>{
  const [data,setData] = useState(null)
  const [error,setError] = useState(null)
  const [loading,setLoading] = useState(false)
  const [shouldRefetch, refetch] = useState({}); 
  useEffect(() => {
    (
      async function(){
        try{
          setLoading(true)
          const response = await axios.get(url);
          setData(response.data)
        }catch(err){
            setError(err)
        }finally{
            setLoading(false)
        }
      }
    )()
  }, [url, shouldRefetch]);

  return { data, error, loading, refetch}
}

export default useFetch;