import React from 'react';
import DashboardContainer from './Containers/DashboardContainer';
import TodoContainer from './Containers/TodoContainer';
import AppLayout from './Layout/App';
import RouteLayout from './Layout/Route';
import Page01 from './Pages/Page01';
import Page02 from './Pages/Page02';
import Page03 from './Pages/Page03';

const AppRouter = () => {
  return (
    <React.Fragment>
      <RouteLayout exact layout={AppLayout} path="/" component={DashboardContainer} />
      <RouteLayout layout={AppLayout} path="/page1" component={Page01} />
      <RouteLayout layout={AppLayout} path="/page2" component={Page02} />
      <RouteLayout layout={AppLayout} path="/page3" component={Page03} />
      <RouteLayout layout={AppLayout} path="/todos" component={TodoContainer} />
    </React.Fragment> 
  )
};

export default AppRouter;